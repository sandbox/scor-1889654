<?php

define('DRUPAL_ROOT', getcwd());

require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

ini_set('display_errors', TRUE);

include(DRUPAL_ROOT . '/includes/xmlrpc.inc');

// Need to trim whitespace, or the key doesn't match up to a client
$key = trim(file_get_contents('key.txt'));

  $batch = array(
    'branches' => array(),
    'files' => array(),
    'projects' => array(),
  );

  $batch['projects'][] = array(
    'client_identifier' => 3060,
    'name' => 'Drupal core (security testing)',
    'repository_type' => 'git',
    'repository_url' => 'git://git.drupal.org/project/drupal.git',
    'link' => 'http://code.example.com',
  );

  $batch['branches'][] = array(
    // Project identifier represents the Project NID on drupal.org
    'project_identifier' => 3060,
    // Client identifier represents the Release NID on drupal.org
    //'client_identifier' => 572834,      // 8.x-dev
    'client_identifier' => 156281,        // 7.x-dev
    //'client_identifier' => 1870572,     // 7.18
    //'client_identifier' => 97386,       // 6.x-dev
    // VCS identifier represents the actual branch / tag name to checkout
    'vcs_identifier' => '7.x',
    'plugin_argument' => array(
      // 'drupal.core.version' => '8',
      'drupal.core.version' => '7',
      // 'drupal.core.version' => '6',
    ),
    'test' => FALSE,
    'link' => 'http://code.example.com/example-branch',
  );

  $batch['files'][] = array(
    // Branch identifier is the same as 'client identifier' above
    'branch_identifier' => 156281,
    // Client identifier is a unique identifier for this file (fid)
    // Change this is you want to have a new test id on qa.d.o, otherwise it
    // will override the previous results.
    // Generate random id large enough to not conflict with d.o nids.
    'client_identifier' => mt_rand(19487300, 999999999),
    'file_url' => 'http://drupal.org/files/dropbutton-1799498-45.patch',
  );

echo "Sending request to http://qa.drupal.org/xmlrpc.php\n";
// Need to prefix http auth username/password for staging
// Modified to use D6 xmlrpc function signature
if ($response = xmlrpc('http://qa.drupal.org/xmlrpc.php', 'pifr.queue', $key, $batch)) {

  foreach (array_merge($response['branches'], $response['files']) as $test) {
    $tests[] = l('#' . $test, 'http://qa.drupal.org/pifr/test/' . $test);
  }

  if ($tests) {
    echo theme('item_list', $tests);
  }
  else {
    echo 'No test(s) queued. The tests are most likely already queued.';
  }
}
else {
  echo("XMLRPC Call Failed");
}

// The response is something like this,
// http://qa.drupal.org/pifr/test/377058
// Array
// (
//     [branches] => Array
//         (
//             [716] => 377053
//         )
//
//     [files] => Array
//         (
//             [19487298] => 377058
//         )
//
// )

